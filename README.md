Ansible playbook name
===========================================

Requirements
------------

- Ansible: 2.7+
- Boto3 Python library: latest availble

Usage
-----

- install roles:

```sh
ansible-galaxy role install -f -r requirements.yml
```

- verify your access to nodes (master key is required):

```sh
ansible -m ping --private-key "<path to ssh key>" -u ec2-user "tag_Name_domain_prod*"
```

- re-verify the list of hosts affected by the playbook:

```sh
ansible-playbook playbook.yml --private-key "<path to ssh key>" --list-hosts
```

- launch playbook:

```sh
ansible-playbook playbook.yml --private-key "<path to ssh key>"
```
